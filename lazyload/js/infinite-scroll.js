import AxiosReq from "./base/https";
let article_box = document.querySelector(".article-box__main");
var page = 1;

//0729 ie的進度條
var versions = (function () {
  var u = navigator.userAgent,
    app = navigator.appVersion;
  var ua = navigator.userAgent.toLowerCase();
  return {
    trident: u.indexOf("Trident") > -1, //IE
  };
})();

// 進度條
$(window).on("scroll", function () {
  if (versions.trident) {
    var winScroll =
      document.body.scrollTop || document.documentElement.scrollTop;
    var height =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("pageBar").style.width = scrolled + "%";
  }
  var scroll = $(window).scrollTop();
  var second_art_area = $(".second_art_area").offset().top;
  var second_art_area_height = $(".second_art_area").height();
  if (scroll < second_art_area - 100) {
    var winScroll = document.documentElement.scrollTop;
    var height = second_art_area - 72;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("pageBar").style.width = scrolled + "%";
    document.title = infiniteScroll.seo_title;

    history.replaceState('', infiniteScroll.title, infiniteScroll.url);

    $(".icon-facebook").attr(
      "href",
      "https://www.facebook.com/sharer/sharer.php?u=" +
      encodeURI(infiniteScroll.url)
    );
    $(".icon-line").attr(
      "href",
      "https://line.naver.jp/R/msg/text/?" +
      encodeURI(infiniteScroll.url + "\n" + infiniteScroll.title)
    );
    $(".icon-twitter").attr(
      "href",
      "https://twitter.com/share?url=" +
      encodeURI(infiniteScroll.url) +
      ";text=" +
      encodeURI(infiniteScroll.title)
    );
    $(".icon-mail").attr(
      "href",
      "mailto:?subject=" +
      encodeURI(infiniteScroll.title) +
      "&body=" +
      encodeURI(infiniteScroll.url)
    );
  } else {
    var winScroll2 = document.documentElement.scrollTop - second_art_area + 120;
    var height2 =
      $("body").height() - second_art_area - window.innerHeight + 120;
    var scrolled2 = (winScroll2 / height2) * 100;
    document.getElementById("pageBar").style.width = scrolled2 + "%";
    if (second_art_area > 0) {
      document.title = seo_title;
      if (page == 1) {
        page = 2;
        window.history.pushState(null, second_title, second_url);
      }
      history.replaceState('', second_title, second_url);

      $(".icon-facebook").attr(
        "href",
        "https://www.facebook.com/sharer/sharer.php?u=" + encodeURI(second_url)
      );
      $(".icon-line").attr(
        "href",
        "https://line.naver.jp/R/msg/text/?" +
        encodeURI(second_url + "\n" + second_title)
      );
      $(".icon-twitter").attr(
        "href",
        "https://twitter.com/share?url=" +
        encodeURI(second_url) +
        ";text=" +
        encodeURI(second_title)
      );
      $(".icon-mail").attr(
        "href",
        "mailto:?subject=" +
        encodeURI(second_title) +
        "&body=" +
        encodeURI(second_url)
      );
      $("#second_art_area").attr("data-href", encodeURI(second_url));
    }
  }
});

if (window.innerWidth > 768) {
  let side_content = document.querySelector(".side-content");
  let side_content_div = document.querySelectorAll(".side-content > div");
  if (side_content_div.length == 4) {
    $(".side-content > div:nth-child(4)").css({
      position: "sticky",
      top: "90px",
    });
  }
  if (side_content_div.length == 6) {
    let sticky_con = document.createElement("div");
    sticky_con.setAttribute("class", "sticky_con");
    $(".side-content > div:nth-child(5)").appendTo(sticky_con);
    $(".side-content > div:nth-child(5)").appendTo(sticky_con);
    side_content.appendChild(sticky_con);
    sticky_con.style.position = "sticky";
    sticky_con.style.top = "70px";
  }
  if (side_content_div.length == 7) {
    $(".side-content > div:nth-child(7)").css({
      position: "sticky",
      top: "90px",
    });
  }
  if (side_content_div.length == 9) {
    let sticky_con = document.createElement("div");
    sticky_con.setAttribute("class", "sticky_con");
    $(".side-content > div:nth-child(8)").appendTo(sticky_con);
    $(".side-content > div:nth-child(8)").appendTo(sticky_con);
    side_content.appendChild(sticky_con);
    sticky_con.style.position = "sticky";
    sticky_con.style.top = "70px";
  }
}

window.onload = function () {
  //0821
  if (window.innerWidth > 768) {
    $(".side-content").css(
      "height",
      $(".article-box__main").height() - 48 + "px"
    );
  }
};

let second_title = "";
let second_url = "";
let seo_title = "";
let art_num = 0,
  art_num_limited = 1;

if (art_num_limited == 1 && infiniteScroll.id != "" && infiniteScroll.show != "") {
  AxiosReq(
    "GET",
    `${process.env.MIX_APP_URL}/api/v1.0/recommend/` + infiniteScroll.id
  )
    .then(function (response) {
      const callback = (entries, observer) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            try {
              if (art_num <= art_num_limited && response.data.success) {
                //第二篇文章複製內容
                let article_box = document.querySelector(".article-box__main");
                let breadcrumb = document.querySelector(".breadcrumb");
                let article_titleBar = document.querySelector(
                  ".article-titlebar"
                );
                let article_box_clone = article_box.cloneNode(true);
                let breadcrumb_clone = breadcrumb.cloneNode(true);
                let article_titleBar_clone = article_titleBar.cloneNode(true);
                let second_art_area = document.querySelector(
                  ".second_art_area"
                );
                let data = response.data.items;
                second_art_area.appendChild(breadcrumb_clone);
                second_art_area.appendChild(article_titleBar_clone);
                second_art_area.appendChild(article_box_clone);
                second_title = data.title;
                second_url = data.url;
                seo_title = data.seo_title;

                //替換主圖 0821
                $(".second_art_area .main-img__pic img").attr(
                  "src",
                  data.photo
                );


                //替換主圖alt 0821
                $(".second_art_area .main-img__pic img").attr(
                  "alt",
                  data.photo_illustrate
                );

                if (Array.isArray(data.breadcrumb)) {
                  let breadcrumb = "";
                  data.breadcrumb.forEach(function (value) {
                    breadcrumb +=
                      '<li><a href="' +
                      value.path +
                      '">' +
                      value.name +
                      "</a></li>";
                  });
                  $(".second_art_area .breadcrumb").html(
                    '<div class="container"><nav aria-label="breadcrumb"><ul>' +
                    breadcrumb +
                    "</ul></nav></div>"
                  );
                }

                //替換第二篇 h1 response.data.msg
                $(".second_art_area .article-titlebar .title h1").html(
                  data.title
                );
                //替換第二篇 response.data.msg
                $(
                  ".second_art_area .article-titlebar .info .info__author"
                ).html(
                  '<a href="' +
                  data.author_url +
                  '">' +
                  data.author.ownerName +
                  "</a>"
                );
                //替換第二篇日期 response.data.msg
                $(".second_art_area .article-titlebar .info .info__date").html(
                  '<i class="icon-date"></i>' + data.date
                );
                //替換第二篇觀看次數 response.data.msg
                $(".second_art_area .article-titlebar .info .info__view").html(
                  '<i class="icon-view"></i>' + data.view
                );

                $(".second_art_area .breadcrumb .container").removeClass(
                  "container"
                );
                $(".second_art_area .article-titlebar .container").removeClass(
                  "container"
                );

                //替換圖說（右）
                $(".second_art_area .main-img__creadit").html(
                  "Photo Credit：" + data.photo_grapher
                );

                //替換前言
                $(".second_art_area .main-introduction").html(data.description);

                //替換第二篇art內容
                $(
                  ".second_art_area .main-article-box article .trackSection"
                ).html(data.content);

                if (Array.isArray(data.keywords)) {
                  let articleTag = "";
                  data.keywords.forEach(function (keyword) {
                    articleTag +=
                      '<li><a href="' +
                      data.keywords_url +
                      "?key=" +
                      keyword +
                      '">' +
                      keyword +
                      "</a></li>";
                  });

                  //替換關鍵字
                  $(".second_art_area .article-tag").html(
                    "<ul>" + articleTag + "</ul>"
                  );
                }

                if (Array.isArray(data.external.related)) {
                  let related = "";
                  data.external.related.forEach(function (value) {
                    related +=
                      '<li><a href="' +
                      value.url +
                      '">' +
                      value.title +
                      "</a></li>";
                  });

                  //替換關聯閱讀 (不知道你們要怎麼拆，先整塊替換，如果需要細分再麻煩你們拆開來)
                  $(".second_art_area .related .article-links__content").html(
                    related
                  );
                }

                if (Array.isArray(data.external.related)) {
                  let related = "";
                  data.external.related.forEach(function (value) {
                    related +=
                      '<li><a href="' +
                      value.url +
                      '">' +
                      value.title +
                      "</a></li>";
                  });

                  //替換關聯閱讀 (不知道你們要怎麼拆，先整塊替換，如果需要細分再麻煩你們拆開來)
                  $(".second_art_area .related .article-links__content").html(
                    related
                  );
                }

                if (Array.isArray(data.external.recommend)) {
                  let recommend = "";
                  data.external.recommend.forEach(function (value) {
                    recommend +=
                      '<li><a href="' +
                      value.url +
                      '">' +
                      value.title +
                      "</a></li>";
                  });

                  //替換作品推薦 (不知道你們要怎麼拆，先整塊替換，如果需要細分再麻煩你們拆開來)
                  $(".second_art_area .recommend .article-links__content").html(
                    recommend
                  );
                }

                if (Array.isArray(data.external.reference)) {
                  let reference = "";
                  data.external.reference.forEach(function (value) {
                    reference +=
                      '<li><a href="' +
                      value.title +
                      '">' +
                      value.url +
                      "</a></li>";
                  });

                  //替換參考資料 (不知道你們要怎麼拆，先整塊替換，如果需要細分再麻煩你們拆開來)
                  $(".second_art_area .reference .article-links__content").html(
                    reference
                  );
                }

                //替換作者 (不知道你們要怎麼拆，先整塊替換，如果需要細分再麻煩你們拆開來)
                $(".second_art_area .article-author").html(
                  '<div class="article-author__img"><picture class="picture lr11"><div class="mask"></div><img data-src="' +
                  data.author.ownerPhoto +
                  '" class="img lazy" alt="' +
                  data.author.ownerName +
                  '"><noscript><img src="' +
                  data.author.ownerPhoto +
                  '" class="img" alt="' +
                  data.author.ownerName +
                  '"></noscript></picture><img src="' +
                  data.author.ownerPhoto +
                  '" class="img ieonly" alt="' +
                  data.author.ownerName +
                  '"> </div>	<div class="article-author__content"><div class="title">' +
                  data.author.ownerName +
                  '</div><div class="foreword">' +
                  data.author.ownerInfo +
                  '</div><div class="btns"><div id="author-more__btn" class="btn btn--small btn--text">看更多<i class="icon-btn-arrow primary"></i></div></div></div>'
                );

                $(".second_art_area .article-author").attr(
                  "onclick",
                  "location.href='" + data.author_url + "'"
                );

                //goTop事件註冊
                $(".icon-goTop").click(function () {
                  $("html, body").animate({ scrollTop: 0 }, 500);
                  return false;
                });

                //刪除第二篇不需要的元素
                var self = document.querySelector(
                  ".main-article .article-box__side"
                );
                if (self) {
                  self.parentNode.removeChild(self);
                }

                var self2 = document.querySelector(
                  ".main-article .article-shortcut"
                );
                if (self2.parentNode) {
                  self2.parentNode.removeChild(self2);
                }

                var self3 = document.querySelector(
                  ".main-article .article-interest"
                );
                if (self3) {
                  self3.parentNode.removeChild(self3);
                }
                var self4 = document.querySelector(
                  ".main-article .article-video"
                );
                if (self4) {
                  self4.parentNode.removeChild(self4);
                }
                var self5 = document.querySelector(".main-article .ads-box");
                if (self5) {
                  self5.parentNode.removeChild(self5);
                }

                var self6 = document.querySelector(
                  ".second_art_area .second_art_area"
                );
                if (self6) {
                  self6.parentNode.removeChild(self6);
                }

                var self7 = document.querySelector(
                  ".second_art_area .article-titlebar .category"
                );
                if (self7) {
                  self7.parentNode.removeChild(self7);
                }

                if (data.is_ad) {
                  $(".second_art_area .article-titlebar").prepend(
                    '<div class="category" data-sponsor="贊助"></div>'
                  );
                }


                
                $("#second_art_area .article-box__main").css("margin-right", "0");

                art_num++;

                $("#second_art_area").css("display", "block");

                //0821
                if (window.innerWidth > 768) {
                  for (let i = 1; i <= 30; i++) {
                    setTimeout(function () {
                      $(".side-content").css(
                        "height",
                        $(".article-box__main").height() - 48 + "px"
                      );
                    }, 100 * i);
                  }
                }
              }

              if (art_num == art_num_limited) {
                observer.unobserve(target);
              }

              $(".second_art_area .item a").on("click", function () {
                window.location.href = $(this).attr("data-url");
              });

              $(".second_art_area .card").on("click", function () {
                window.location.href = $(this).attr("data-link");
              });
            } catch (e) {
              console.log(e);
            }
          }
        });
      };

      const observer = new IntersectionObserver(callback);
      const target = document.querySelector(".article-author");
      observer.observe(target);
    })
    .catch(function (error) {
      console.log(error);
    })
    .finally(function () { });
}
